import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  constructor(private http: HttpClient) {
  }

  getVideoObject(link: string): Observable<any> {
    return this.http.get(link.replace(/https?:\/\/(www.)?youtube.com/gi, '/api'), {
      responseType: 'text'
    });
  }
}
