import { IVideoProperties } from '../interfaces/interfaces';

export class VideoProperties implements IVideoProperties {
  private _videos: string[] = [];
  private _thumbnailUrl: string;
  private _title: string;
  private _videoId: string;

  get videoId(): string {
    return this._videoId;
  }

  set videoId(value: string) {
    this._videoId = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get thumbnailUrl(): string {
    return this._thumbnailUrl;
  }

  set thumbnailUrl(value: string) {
    this._thumbnailUrl = value;
  }

  get videos(): string[] {
    return this._videos;
  }

  pushVideo(videoObj: any) {
    this._videos.push(videoObj);
  }
}
