export interface IYoutubeRawData {
  url_encoded_fmt_stream_map: any;
  thumbnail_url: string;
  title: string;
  videos: string[];
  video_id: string;
}

export interface IYoutubeData {
  ext: any;
  type: any;
}

export interface IVideoProperties {
  videos: string[];
  videoId: string;
  thumbnailUrl: string;
  title: string;

  pushVideo(videoUrl: string): void;
}
