import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { YoutubeService } from './Services/youtube.service';
import { IVideoProperties, IYoutubeData, IYoutubeRawData } from './interfaces/interfaces';
import { VideoProperties } from './models/video-properties';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  form: FormGroup;
  videoProperties: IVideoProperties;
  videoDownloadParentUrl = 'https://www.youtube.com/get_video_info?html5=1&fmt=18&asv=2&hd=1&video_id=';

  path = 'https://www.youtube.com/watch?v=GTE7JUKNfLY';
  imageParentPreviewUrl = 'https://img.youtube.com/vi/';

  constructor(private formBuilder: FormBuilder,
              private youtubeService: YoutubeService) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      'link': [this.path, Validators.required]
    });
  }

  formSubmit() {
    const url = this.form.value['link'];
    const videoId = this.getVideoId(url);

    this.parsePath(this.videoDownloadParentUrl + videoId);
  }

  toJson(data): any {
    const obj = {};
    data.split('&').forEach((v, k) => {
      const spl = v.split('=');
      obj[spl[0]] = decodeURIComponent(spl[1]);
    });
    return obj;
  }

  getVideoPreviewUrl(videoId: string) {
    return `${this.imageParentPreviewUrl}${videoId}/0.jpg`;
  }

  getVideoId(rawPath: string): string {
    const regex = /v=([a-zA-Z0-9]+)/gi;
    return regex.exec(rawPath)[1];
  }

  parsePath(link: string) {
    this.videoProperties = new VideoProperties();

    this.youtubeService.getVideoObject(link).subscribe(data => {
      const obj = this.toJson(data) as IYoutubeRawData;
      const tmp = obj.url_encoded_fmt_stream_map.split(',');

      this.videoProperties.title = obj.title.replace(/\+/g, ' ');
      this.videoProperties.thumbnailUrl = this.getVideoPreviewUrl(obj.video_id);
      this.videoProperties.videoId = obj.video_id;

      for (let tmpItem of tmp) {
        tmpItem = this.toJson(tmpItem) as IYoutubeData;
        tmpItem.ext = tmpItem.type
          .match(/^video\/\w+(?=;)/g)[0]
          .replace(/^video\//, '');

        this.videoProperties.pushVideo(tmpItem);
      }
    });
  }
}
